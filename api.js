const { Router } = require("express");
const Gambler = require("./Gambler");


const apiRouter = Router();


apiRouter.post("/user/", async (req, res) => {
    try {
        const newGambler = await Gambler({ ...req.body });
        await newGambler.save();
        res.json({ id: newGambler._id });
    } catch(err) {
        console.error(err);
        res.sendStatus(400);
    }
});


apiRouter.get("/user/:id", async (req, res) => {
    try {
        const searchedGambler = await Gambler.findById(req.params.id);
        const pojoGambler = searchedGambler.toObject();
        delete pojoGambler.password;
        console.log("2", pojoGambler);
        res.json(pojoGambler);
    } catch (err) {
        console.error(err);
        res.sentStatus(400);
    }
});





apiRouter.patch("/user/:id", async (req, res) => {
    try {
        const users = db.collection("gamblers");
        if (req.body.username) {
            const username = req.body.username;
            const updatedUser = await users.updateOne(
                {
                    _id: new ObjectId(req.params.id),
                },
                {
                    $set: { username } // { "username" : username}
                }
            );
            res.json(updatedUser);
        } else {
            res.sentStatus(400);
        }

    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});


apiRouter.delete("/user/:id", async (req, res) => {
    try {
      const users = db.collection("gamblers");
      const deletedUSer = await users.deleteOne({
        _id: new ObjectId(req.params.id),
      });
      res.json(deletedUSer);
    } catch (err) {
      console.error(err);
      res.sendStatus(500);
    }
  });
  

module.exports = apiRouter; 

