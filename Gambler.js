const mongoose = require("mongoose");

const gamblerSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  fullName: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  bankroll: { type: Number, required: true },
  birthDate: { type: Date, required: true },
});

const Gambler = mongoose.model("Gambler", gamblerSchema);
module.exports = Gambler;
